package Elevator;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Ironhide
 * @since 2020-11-18
 */
class ElevatorThread implements Runnable {

    private Elevator elevator;
    private ReentrantLock lock;
    private Condition personCondition;
    private Condition elevatorCondition;

    ElevatorThread(Elevator elevator, ReentrantLock lock, Condition personCondition, Condition elevatorCondition) {
        this.elevator = elevator;
        this.lock = lock;
        this.personCondition = personCondition;
        this.elevatorCondition = elevatorCondition;
    }

    @Override
    public void run() {
        while (true) {
            lock.lock();
            try {
                if (elevator.getEventCount() != 0) {
                    int eTarget = elevator.getNextTarget();

                    while (eTarget != elevator.geteFloor()) {
                        //上一层楼睡一秒钟
                        TimeUnit.SECONDS.sleep(1);

                        int eTargetTemp;
                        if (elevator.isDirection()) {
                            //例：当电梯在1层，目标为8层时，电梯在上升过程中，5层乘客要上楼，将eTarget改为5；电梯每上一层都获取一次目标
                            if ((eTargetTemp = elevator.getNextTarget()) < eTarget) {
                                eTarget = eTargetTemp;
                            }

                            elevator.seteFloor(true);
                        } else {
                            //同理，电梯下降过程中，eTargetTemp大于eTarget，则更新
                            if ((eTargetTemp = elevator.getNextTarget()) > eTarget) {
                                eTarget = eTargetTemp;
                            }

                            elevator.seteFloor(false);
                        }

                        System.out.println("电梯方向为" + (elevator.isDirection() ? "上" : "下") + "，目标为" + eTarget + "楼，当前在" + elevator.geteFloor() + "楼");
                    }

                    personCondition.signalAll();
                } else {
                    System.out.println("电梯无目标...");
                }

                elevatorCondition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }
}
