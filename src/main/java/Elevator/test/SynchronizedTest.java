package Elevator.test;

/**
 * @author Ironhide
 * @since 2020-11-13
 */
public class SynchronizedTest {

    /**
     * 载客事件数量，为0表示没有载客需求
     */
    private static int eventCount = 0;

    public static void main(String[] args) {
        Object person = new Object();
        Object ele = new Object();

        new Thread(() -> {
            while (true) {
                synchronized (person) {
                    try {
                        synchronized (ele) {
                            eventCount++;
                            System.out.println("我要坐电梯");
                            ele.notify();
                        }
                        person.wait();

                        synchronized (ele) {
                            System.out.println("我上电梯了，我要去顶楼");
                            ele.notify();
                        }
                        person.wait();

                        eventCount--;
                        System.out.println("我到达目的地了......");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        while (true) {
            synchronized (ele) {
                try {
                    if (eventCount != 0) {
                        synchronized (person) {
                            System.out.println("电梯来接人了");
                            person.notify();
                        }
                        ele.wait();

                        synchronized (person) {
                            System.out.println("电梯去目的地");
                            person.notify();
                        }
                    }

                    ele.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
