package Elevator.test;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Ironhide
 * @since 2020-11-13
 */
public class LockTest {

    /**
     * 载客事件数量，为0表示没有载客需求
     */
    private static int eventCount = 0;

    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        Condition personCondition = lock.newCondition();
        Condition eleCondition = lock.newCondition();

        new Thread(() -> {
            while (true) {

                lock.lock();
                try {
                    ++eventCount;
                    System.out.println("我要坐电梯");

                    eleCondition.signal();
                    personCondition.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }

                System.out.println("我上电梯了，我要去XX楼");

                lock.lock();
                try {
                    eleCondition.signal();
                    personCondition.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }

                --eventCount;
                System.out.println("我到达目的地了......");

            }
        }).start();

        while (true) {
            lock.lock();
            try {
                if (eventCount != 0) {
                    System.out.println("电梯来接人了");
                    personCondition.signal();
                    eleCondition.await();

                    System.out.println("电梯去目的地");
                    personCondition.signal();
                }

                eleCondition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }
}
