package Elevator;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Ironhide
 * @since 2020-11-18
 */
class FloorThread implements Runnable {

    private int pFloor;
    private Elevator elevator;
    private Lock lock;
    private Condition personCondition;
    private Condition elevatorCondition;

    FloorThread(int pFloor, Elevator elevator, ReentrantLock lock, Condition personCondition, Condition elevatorCondition) {
        this.pFloor = pFloor;
        this.elevator = elevator;
        this.lock = lock;
        this.personCondition = personCondition;
        this.elevatorCondition = elevatorCondition;
    }

    @Override
    public void run() {

        //乘客选择上楼/下楼
        boolean isUp;
        if (pFloor == 1) {
            isUp = true;
        } else if (pFloor == elevator.totalFloors) {
            isUp = false;
        } else {
            isUp = new Random().nextInt(2) > 0;
        }

        boolean bn = elevator.pSetUpOrDown(pFloor, isUp);
        System.err.println(pFloor + "楼乘客要" + (isUp ? "上" : "下") + "楼");

        lock.lock();
        try {
            elevatorCondition.signal();
            while (elevator.geteFloor() != pFloor) {
                personCondition.await();
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            lock.unlock();
        }

        //乘客上电梯选择楼层
        int pTarget = new Random().nextInt(elevator.totalFloors) + 1;
        //上楼时，pTarget > floor 下楼 pTarget < floor
        if (isUp) {
            while (pTarget <= pFloor) {
                pTarget = new Random().nextInt(elevator.totalFloors) + 1;
            }
        } else {
            while (pTarget >= pFloor) {
                pTarget = new Random().nextInt(elevator.totalFloors) + 1;
            }
        }

        elevator.setPeopleNum(true);
        elevator.pClearUpOrDown(pFloor, bn);
        elevator.pSetFloor(pTarget, isUp);
        System.err.println(pFloor + "楼乘客上电梯，选择了" + pTarget + "楼，电梯现在" + elevator.getPeopleNum() + "人");

        lock.lock();
        try {
            elevatorCondition.signal();
            while (elevator.geteFloor() != pTarget) {
                personCondition.await();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }

        elevator.pClearFloor(pTarget, isUp);
        elevator.setPeopleNum(false);
        System.err.println(pFloor + "楼乘客下电梯");

        lock.lock();
        try {
            elevatorCondition.signal();
        } finally {
            lock.unlock();
        }
    }
}
