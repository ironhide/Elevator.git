package Elevator;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Ironhide
 * @since 2020-11-12
 */
class Core {

    public static void main(String[] args) throws InterruptedException {

        Elevator elevator = new Elevator(10);

        ReentrantLock lock = new ReentrantLock();
        Condition personCondition = lock.newCondition();
        Condition elevatorCondition = lock.newCondition();

        //直接设置 楼层数+一电梯 个线程
        ExecutorService threadPool = Executors.newFixedThreadPool(elevator.totalFloors + 1);
        threadPool.execute(new ElevatorThread(elevator, lock, personCondition, elevatorCondition));

        for (int i = 1; i <= elevator.totalFloors; i++) {
            TimeUnit.SECONDS.sleep(new Random().nextInt(1) + 1);
            threadPool.execute(new FloorThread(i, elevator, lock, personCondition, elevatorCondition));
        }
    }
}
