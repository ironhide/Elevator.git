package Elevator;

/**
 * @author Ironhide
 * @since 2020-11-12
 * <p>
 * 电梯类
 */
class Elevator {

    /**
     * 总楼层数
     */
    int totalFloors;
    /**
     * 当前电梯所在楼层
     */
    private int eFloor = 1;
    /**
     * 电梯方向，true为上升，反之下降
     */
    private boolean direction = true;
    /**
     * 当前电梯人数
     */
    private int peopleNum = 0;
    /**
     * 载客事件数量，为0表示没有载客需求
     */
    private int eventCount = 0;
    /**
     * 当direction为true，电梯在上升状态时
     * 电梯需要停留的楼层数组，当前数组元素大于0时表示要停留
     * 大于1时表示该层有多个上客或下客需求
     */
    private int[] upTarget;
    /**
     * 当direction为false，电梯在下降状态时电梯需要停留的楼层数组
     * 与upTarget意义一致
     */
    private int[] downTarget;

    Elevator(int totalFloors) {
        this.totalFloors = totalFloors;
        this.upTarget = new int[totalFloors];
        this.downTarget = new int[totalFloors];
    }

    int geteFloor() {
        return eFloor;
    }

    /**
     * 电梯所在楼层的增减
     */
    void seteFloor(boolean bn) {
        eFloor = bn ? eFloor + 1 : eFloor - 1;
    }

    /**
     * 获取电梯下一次要去的层数
     */
    int getNextTarget() {
        //当没有载客事件时，将电梯停到1楼
        if (eventCount == 0) {
            direction = false;
            return 1;
        }

        //当电梯向上到达顶层时/电梯向下到达一层时 换方向
        if (eFloor == totalFloors && isDirection()) {
            setDirection();
        } else if (eFloor == 1 && !isDirection()) {
            setDirection();
        }

        //电梯上升方向
        if (direction) {
            for (int i = eFloor; i < upTarget.length; i++) {
                //例如eFloor为4层，判断第5层是否要上下客，如果有就返回5
                if (upTarget[i] != 0) {
                    return i + 1;
                }
            }

            //防止无限循环
            if (eFloor == 1) {
                return 1;
            }
            //当上升方向没有目标时，转换方向，并递归该方法
            setDirection();
            return getNextTarget();
        } else {
            for (int i = eFloor; i > 1; i--) {
                //例如eFloor为2层，判断第1层是否要上下客，如果有就返回1
                if (downTarget[i - 2] != 0) {
                    return i - 1;
                }
            }

            //当方向为下降且也没有目标时，转换方向，在第一层至电梯当前层遍历upTarget
            for (int i = 0; i < upTarget.length; i++) {
                if (upTarget[i] != 0) {
                    //当前电梯是向下方向，如果目标在电梯层数以上，则要换方向
                    if (i + 1 > eFloor) {
                        direction = true;
                    }
                    return i + 1;
                }
            }

            return 1;
        }
    }

    boolean isDirection() {
        return direction;
    }

    private void setDirection() {
        this.direction = !direction;
    }

    int getEventCount() {
        return eventCount;
    }

    /**
     * bn为true是上客
     * 反之下客
     */
    synchronized void setPeopleNum(boolean bn) {
        peopleNum = bn ? peopleNum + 1 : peopleNum - 1;
    }

    int getPeopleNum() {
        return this.peopleNum;
    }

    /**
     * 乘客选择上楼还是下楼，操作电梯上行下行的数组
     *
     * @param pFloor 乘客所在楼层
     * @param isUp   为true表示乘客要往上走
     * @return 改变的是upTarget数组返回true，反之false，为了方便调用pClearUpOrDown方法
     */
    synchronized boolean pSetUpOrDown(int pFloor, boolean isUp) {
        eventCount++;
        boolean bn = true;

        if (isUp) {
            //电梯向上或向下，人向上
            upTarget[--pFloor]++;
        } else {
            //电梯向上或向下，人向下，两种特殊情况【人所在层数高于电梯所在层数，不管电梯是否上升，都对upTarget++，要先接到人】
            if (pFloor > eFloor) {
                upTarget[--pFloor]++;
            } else {
                //人比电梯低时，在电梯下降时接人
                downTarget[--pFloor]++;
                bn = false;
            }
        }

        return bn;
    }

    /**
     * 乘客进入电梯后清除之前选择上楼下楼的操作
     *
     * @param pFloor 乘客所在楼层
     * @param bn     pSetUpOrDown方法返回值
     */
    synchronized void pClearUpOrDown(int pFloor, boolean bn) {
        if (bn) {
            upTarget[--pFloor]--;
        } else {
            downTarget[--pFloor]--;
        }
    }

    /**
     * 乘客进入电梯后清除之前选择目标楼层
     *
     * @param pTarget 乘客选择的目标楼层
     * @param isUp    乘客选择上楼下楼
     */
    synchronized void pSetFloor(int pTarget, boolean isUp) {
        if (isUp) {
            upTarget[--pTarget]++;
        } else {
            downTarget[--pTarget]++;
        }
    }

    /**
     * 乘客下电梯时，清除该乘客的事件
     * 对电梯上行下行数组进行-1操作
     */
    synchronized void pClearFloor(int pTarget, boolean isUp) {
        eventCount--;

        if (isUp) {
            upTarget[--pTarget]--;
        } else {
            downTarget[--pTarget]--;
        }
    }

}
